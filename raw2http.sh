#!/bin/bash

# ----------------------------------------------------------------------------------------
# INSTRUCTIONS
# ----------------------------------------------------------------------------------------
#
# This script reads raw TCP/IP packets and sends an HTTP GET request to Telegram server.
#
# The requested raw TCP/IP packet syntax is <method> <token> <chat_id> <photo_url_optional> <text> 
#
# eg. message 123456:acbdefghi1234567890 26374837 example

port=8083                                                   # listen port

while true; do

    incoming=$(nc -l -p $port)                              # reads the text sent by the camera and stores it into a variable
    check=$?                                                # acquires boolean output of the previous statement

    if [ $check == 0 ] ; then                               # if $incoming is not null
        keyword=$(echo $incoming | awk '{print $1}')        # reads method keyword
        token=$(echo $incoming | awk '{print $2}')          # reads bot token
        chat_id=$(echo $incoming | awk '{print $3}')        # reads chat ID


        if [ "$keyword" == "photo" ] ; then                 # if a sendPhoto method is requested
            photo_url=$(echo $incoming | awk '{print $4}')  # reads photo url
            text=$(echo $incoming |  awk '{for (i=4; i<NF; i++) printf $i " "; if (NF >= 5) print $NF; }')  # reads text
            curl -g "${target_proto}://api.telegram.org/bot${token}/sendPhoto?chat_id=${chat_id}&photo=${photo_url}&caption=\"${text}\"&parse_mode=Markdown"    # sends photo
        fi


        if [ "$keyword" == "message" ] ; then               # if a sendMessage method is requested
            text=$(echo $incoming |  awk '{for (i=4; i<NF; i++) printf $i " "; if (NF >= 4) print $NF; }')  # reads text
            curl -g "${target_proto}://api.telegram.org/bot${token}/sendMessage?chat_id=${chat_id}&text=${text}&parse_mode=Markdown"    # sends message
        fi


    fi
    sleep 2
done
